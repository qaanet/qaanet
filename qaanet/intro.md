Created: 12/13/2021
Last updated: 12/13/2021

# What is qaanet?

**qaanet** is the working title of a crypto project. On these pages the concept for said project is laid out (in all its prematureness).
qaanet stands for **q**uestion **a**nd **a**nswer **net**. The product of this concept - if brought into existence - is a platform and framework for a new network of question and answer sites.

Question and answer sites are a handy way of giving very specific and concise information about a question or problem quickly to a sites visitors thus providing great value. 

In typical web2 fashion Q&A sites' contents are usually created and moderated by users who are incentivized with elements of gamification (points, reputation, badges and such) and with the potential use of their contributions in a resume of their work later on. The financial gains of such a site usually end up with the company running the site. 

qaanet explores a new, additional method of rewarding users for their work on question and answer sites.
The idea of incentivizing content creators on question and answer sites is not new. Examples exist of sites which pay content creators in gift cards or in fiat directly. What qaanet is exploring is the use of web3 
methods of letting content creators earn a share of the sites value by rewarding them with payments in a crypto token.


